﻿#include"Authorization.h"
#include<string>
#include<iomanip>

const string FILE_OF_BOOKS = "Books.txt";

struct Books
{
	int registration_number;
	string Author;
	string book_title;
	int  year;
	string publishing;
	int  number_of_pages;
	string number_of_library_card;
	string mark;             //0 - library, 1 - reader
};

void readFileOfBooks(Books** arr_of_accounts, int& number_of_accounts);
void EditingMode(Books** arr_of_accounts, int& number_of_accounts);
void ShowAllBooks(Books* arr_of_accounts, int& number_of_accounts);
void ReplaceSymbol(string& str, char symbol, char newSymbol);
string Protection();
void Text(string& AddAuthor, int& AddYear, string& AddPublishing, int& AddNumberOfPages, string& AddLibraryCard);
Books* getRememberForArray(Books* arr_of_accounts, int& number_of_accounts, int new_number);
void AddABook(Books* arr_of_accounts, int& number_of_accounts);
void WriteNewArrayInBookFile(Books* arr_of_accounts, int& number_of_accounts);
int GetSizeOfArrayOfBooks();

int main()
{
	string AddName, AddAuthor, AddPublishing, AddLibraryCard;
	int AddNumberOfPages, AddYear;
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int number_of_books = GetSizeOfArrayOfBooks();
	Books* arr_of_books = new Books[number_of_books];
	readFileOfBooks(&arr_of_books, number_of_books);
	EditingMode(&arr_of_books, number_of_books);
	WriteNewArrayInBookFile(arr_of_books, number_of_books);
	delete[]arr_of_books;
	return 0;
}

int GetSizeOfArrayOfBooks()
{
	int number = 0;
	string str;
	ifstream fin(FILE_OF_BOOKS, ios::in);
	if (fin.is_open())
	{
		if (fin.peek() == ifstream::traits_type::eof())
		{
			number = 0;
		}
		else
		{
			while (getline(fin, str))
			{
				number++;
			}
		}
	}
	else
	{
		number = 0;
	}
	fin.close();
	return number;
}

void readFileOfBooks(Books** arr_of_accounts, int& number_of_accounts)
{

	if (number_of_accounts == 0)
	{
		ofstream fout(FILE_OF_BOOKS);         //open file for writing
		if (!fout.is_open())
		{
			cout << "File opening error" << endl;
			return;
		}
		else
		{	
			cout << "                                ================================================================" << endl;
			cout << "                                |             The file is empty. Please add a book!            |" << endl;
			cout << "                                ================================================================" << endl;
			*arr_of_accounts = getRememberForArray(*arr_of_accounts, number_of_accounts, number_of_accounts + 1);
			AddABook(*arr_of_accounts, number_of_accounts);
			fout.close();
		}
	}
	else
	{
		ifstream fin(FILE_OF_BOOKS, ios::in);       //open file for reading
		if (!fin.is_open())
		{
			cout << "File opening error" << endl;
			return;
		}
		int i = 0;
		while (!fin.eof() && fin.peek() != ifstream::traits_type::eof())
		{
			fin >> (*arr_of_accounts)[i].registration_number >>(*arr_of_accounts)[i].Author>> (*arr_of_accounts)[i].book_title
				>> (*arr_of_accounts)[i].year>> (*arr_of_accounts)[i].publishing >> (*arr_of_accounts)[i].number_of_pages
				>> (*arr_of_accounts)[i].number_of_library_card >> (*arr_of_accounts)[i].mark;
			ReplaceSymbol((*arr_of_accounts)[i].Author, '*', ' ');
			ReplaceSymbol((*arr_of_accounts)[i].publishing, '*', ' ');
			ReplaceSymbol((*arr_of_accounts)[i].book_title, '*', ' ');
			i++;
		}
		fin.close();
	}
}

void EditingMode(Books** arr_of_books, int& number_of_books)
{
	int chose;
	bool cycle = true;
	while (cycle)
	{
		cout << "                                ================================================================" << endl;
		cout << "                                |                    1 - To view the accounts                  |" << endl;
		cout << "                                |                    2 - Add an account                        |" << endl;
		cout << "                                |                    3 - Edit an account                       |" << endl;
		cout << "                                |                    4 - Delete an account                     |" << endl;
		cout << "                                |                    0 - Return back                           |" << endl;
		cout << "                                                           ->";
		chose = LetterProtection();
		switch (chose)
		{
		case 1:ShowAllBooks(*arr_of_books, number_of_books);
			break;
		case 2:*arr_of_books = getRememberForArray(*arr_of_books, number_of_books, number_of_books + 1);
			AddABook(*arr_of_books, number_of_books);
			break;
			/*case 3:EditAnAccounts(*arr_of_accounts, number_of_accounts);
				break;
			case 4: DeleteAccount(*arr_of_accounts, number_of_accounts);
				break;*/
		case 0:cycle = false;
			break;
		default:
			cout << "                                        !!!!!!!Enter correct number!!!!!!!               " << endl;
		}
	}
}

void ShowAllBooks(Books* arr_of_accounts, int& number_of_accounts)
{
	system("cls");
	cout << std::setw(19) << "REGISTRATION NUMBER|" << std::setw(13) <<
		"AUTHOR|" << std::setw(22) << "BOOK TITLE|" << std::setw(11) <<
		"YEAR|" << std::setw(15) << "PUBLISHING|" << std::setw(20) <<
		"NUMBER OF PAGES|" << std::setw(24) << "NUMBER OF LIBRARY CARD|" << std::setw(18) << "LOCATION OF BOOK|" << endl;
	for (int i = 0; i < number_of_accounts; i++)
	{
		cout <<"|"<< std::setw(12)<<arr_of_accounts[i].registration_number << 
			std::setw(26) <<arr_of_accounts[i].Author
			<< std::setw(21) << arr_of_accounts[i].book_title <<std::setw(6)
			<< arr_of_accounts[i].year << std::setw(19) << arr_of_accounts[i].publishing
			<< std::setw(9) << arr_of_accounts[i].number_of_pages << std::setw(23) 
			<< arr_of_accounts[i].number_of_library_card<< std::setw(21) << arr_of_accounts[i].mark <<"|"<< endl;
	}
	cout << "                              ====================================================================" << endl;
}

Books* getRememberForArray(Books* arr_of_accounts, int& number_of_accounts, int new_number)
{
	Books* arr_new;
	if (number_of_accounts < new_number)
	{
		arr_new = new Books[new_number];           //create new array
		for (int i = 0; i < number_of_accounts; i++)
		{                                             //rewriting old data into a new array
			arr_new[i].registration_number = arr_of_accounts[i].registration_number;
			arr_new[i].Author = arr_of_accounts[i].Author;
			arr_new[i].book_title = arr_of_accounts[i].book_title;
			arr_new[i].year = arr_of_accounts[i].year;
			arr_new[i].publishing = arr_of_accounts[i].publishing;
			arr_new[i].number_of_pages = arr_of_accounts[i].number_of_pages;
			arr_new[i].number_of_library_card = arr_of_accounts[i].number_of_library_card;
			arr_new[i].mark = arr_of_accounts[i].mark;
		}
		delete[]arr_of_accounts;      //delete old array
		arr_of_accounts = arr_new;
		number_of_accounts = new_number;
	}
	return arr_of_accounts;        //return new array
}

void AddABook(Books* arr_of_accounts, int& number_of_accounts)
{
	string AddName, AddAuthor , AddPublishing , AddLibraryCard ;

	int AddNumberOfPages, AddYear;
	int i = 0;

	cout << "                                ==================================================================" << endl;
	cout << "                                |   if you want to log out of adding an account,enter EXIT       |" << endl;
	cout << "                                |                     Enter book title                           |" << endl;
	cout << "                                                       ->";
	//cin >> AddName;
	flushall();
	getline(cin, AddName);
	while (true)
	{
		if (AddName != "EXIT")
		{
			Text(AddAuthor, AddYear, AddPublishing, AddNumberOfPages, AddLibraryCard);
			arr_of_accounts[number_of_accounts - 1].book_title = AddName;
			arr_of_accounts[number_of_accounts - 1].Author = AddAuthor;
			arr_of_accounts[number_of_accounts - 1].year = AddYear;
			arr_of_accounts[number_of_accounts - 1].publishing = AddPublishing;
			arr_of_accounts[number_of_accounts - 1].number_of_pages = AddNumberOfPages;
			arr_of_accounts[number_of_accounts - 1].number_of_library_card = AddLibraryCard;
			//В это сложно поверить, но именно так выглядит слово библиотека, если открыть его в UTF-8
			//Это простое и быстрое решение, если хочешь - можешь помучиться с более сложными решениями
			arr_of_accounts[number_of_accounts - 1].mark = "áèáëèîòåêà";
			arr_of_accounts[number_of_accounts - 1].registration_number = number_of_accounts;
			break;
		}
		else
		{
			break;
		}
	}
}

void Text(string& AddAuthor, int& AddYear, string& AddPublishing, int& AddNumberOfPages, string& AddLibraryCard)
{

	cout << "                                ==================================================================" << endl;
	cout << "                                |                        Enter the author                        |" << endl;
	cout << "                                                       ->";
	getline(cin, AddAuthor);
	cout << endl;

	cout << "                                ==================================================================" << endl;
	cout << "                                |                         Enter the year                         |" << endl;
	cout << "                                                       ->";
	AddYear = LetterProtection();
	cout << endl;

	cout << "                                ==================================================================" << endl;
	cout << "                                |                         Enter the publishing                   |" << endl;     // ÏÎ×ÅÌÓ ÁËÈÍ ÒÓÒ ÍÅ ÐÀÁÎÒÀÅÒÒÒÒÒÒÒÒ????????

	cout << "                                                       ->";
	//cin >> AddPublishing;

	getline(cin, AddPublishing);
	cout << endl;

	cout << "                                ==================================================================" << endl;
	cout << "                                |                         Enter the number of pages              |" << endl;
	cout << "                                                       ->";
	AddNumberOfPages = LetterProtection();
	cout << endl;

	AddLibraryCard = Protection();
	flushall();
}

string Protection()
{
	string AddCard;
	bool flag = true;
	int i = 0;
	while (flag)
	{
		cout << "                                ==================================================================" << endl;
		cout << "                                |        Enter the library card number of the last reader.       |" << endl;
		cout << "                                                       ->";
		cin >> AddCard;
		while (i < AddCard.length())
		{
			if (AddCard.length() != 6 || isalpha(AddCard[i]))
			{
				cout << "                                ==================================================================" << endl;
				cout << "                                |                   This library card does not exist             |" << endl;
				flag = ChoiceOfAction(flag);
				break;
			}
			else
			{
				if (i == 5)
				{
					flag = false;
				}
				i++;
			}

		}
	}
	return AddCard;
}

void ReplaceSymbol(string& str, char symbol, char newSymbol) {
	for (int i = 0; i < str.length(); i++) {
		if (str[i] == symbol) {
			str[i] = newSymbol;
		}
	}
}

void WriteNewArrayInBookFile(Books* arr_of_accounts, int& number_of_accounts)
{
	ofstream fout(FILE_OF_BOOKS);
	int i;
	if (fout.is_open())
	{
		for (i = 0; i < number_of_accounts; i++)
		{
			ReplaceSymbol(arr_of_accounts[i].Author, ' ', '*');
			ReplaceSymbol(arr_of_accounts[i].book_title, ' ', '*');
			ReplaceSymbol(arr_of_accounts[i].publishing, ' ', '*');
			fout << arr_of_accounts[i].registration_number << '\t'
				<< arr_of_accounts[i].Author << '\t'
				<< arr_of_accounts[i].book_title << '\t'
				<< arr_of_accounts[i].year << '\t'
				<< arr_of_accounts[i].publishing << '\t'
				<< arr_of_accounts[i].number_of_pages << '\t'
				<< arr_of_accounts[i].number_of_library_card << '\t'
				<< arr_of_accounts[i].mark;
			if (i < number_of_accounts - 1)
			{
				fout << endl;
			}

		}
		
		fout.close();
	}
}
/*Íå ðàáîòàåò íîðìàëüíîå äîáàâëåíèå àêêàóíòà: ß ââîæó íàçâàíèå êíèãè "Ïîäíÿòàÿ Öåëèíà", à íà ýêðàí âûâîäèò òîëüêî "Öåëèíà". È òàê ñî âñåìè ñòðîêàìè ñîñòîÿùèìè èç íåñêîëüêèõ ñëîâ.
Ìíîãîâåðîÿòíî, ÷òî ÿ íåïðàâèëüíî èñïîëüçóþ getline(), íî ÿ íå ïîíèìàþ, â ÷åì êîíêðåòíî îøèáêà. 
Âûâîäèòñÿ îøèáêà òîãäà, êîãäà ÿ ñîçäàþ àáñîëþòíî íîâûé ôàéë è çàïèñûâàþ òóäà èíôîðìàöèþ, à ïîòîì ïðîñìàòðèâàþ àêêàóíòû (ïðîáëåìà áûëà ñ ñèìâîëàìè'*'è ' ' .
Íå ðàáîòàåò ñòðîêà EXIT, åñëèÿ õî÷ó âûéòè èç äîáàâëåíèÿ ó÷åòíîé çàïèñè. Íó,à òàêæå, ïðîáëåìû ñ êîäèðîâêîé, êîòîðûå ÿ ïûòàëàü ðåøèòü, íî ó ìåíÿ íå ïîëó÷èëîñü. 
Ôàéë â êîäèðîâêå UTF-8, íî îí óæå ïóñòîé*/


