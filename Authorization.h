#pragma once

#include<iostream>
#include<string>
#include<fstream>
#include<Windows.h>
#include<conio.h>
using namespace std;

const string FILE_OF_PASSWORLD = "Accounts.txt";

struct Accounts
{
	string login;
	string password;
	int role;                 //0-admin 1-user 
	int access;               //0-working condition 1-in the waiting state
};
//WORK WITH ACCOUNTS
int GetSizeOfArray();
void readFile(Accounts* arr_of_accounts, int& number_of_accounts);
Accounts* MenuOfAuthorization(Accounts* arr_of_accounts, int& number_of_accounts);
int LetterProtection();
void login(Accounts** arr_of_accounts, int& number_of_accounts);
string password();
int EnterLogin(bool& correct_login, string login, Accounts* arr_of_accounts, int& number_of_accounts);
bool ChoiceOfAction(bool& correct_login);
void menu(Accounts** arr_of_accounts, int& number_of_accounts, int role);
Accounts* getRememberForArray(Accounts* arr_of_accounts, int& number_of_accounts, int new_number);
void AddAnAccount(Accounts* arr_of_accounts, int& number_of_accounts);
void menu(Accounts** arr_of_accounts, int& number_of_accounts, int role);
void AdminMenu(Accounts** arr_of_accounts, int& number_of_accounts);
void AccountManagement(Accounts** arr_of_accounts, int& number_of_accounts);
void ToViewTheAccounts(Accounts* arr_of_accounts, int& number_of_accounts);
void EditAnAccounts(Accounts* arr_of_accounts, int& number_of_accounts);
string ChangeLogin(Accounts* arr_of_accounts, int& number_of_accounts);
string ChangePassword();
void ChangeRoleOrAccess(Accounts* arr_of_accounts, int& number_of_accounts, bool role, int& i);
void DeleteAccount(Accounts* arr_of_accounts, int& number_of_accounts);
void WriteNewArrayInFile(Accounts* arr_of_accounts, int& number_of_accounts);


